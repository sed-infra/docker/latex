FROM debian
LABEL maintainer="Cédric FARINAZZO <cedric.farinazzo@gmail.com>"

ENV DEBIAN_FRONTEND noninteractive

RUN mkdir -p /source
WORKDIR /source

RUN apt-get update && apt-get install -y texlive-full
